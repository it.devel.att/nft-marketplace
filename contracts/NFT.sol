//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

interface IERC721Mintable is IERC721 {
    function mint(address to, string memory uri) external;
}

contract NFT is IERC721Mintable, ERC721, ERC721URIStorage {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;
    address private _marketPlace;
    address private _admin;

    constructor() ERC721("MarketplaceToken", "MTT") {
        _admin = msg.sender;
    }

    function setMarketplace(address marketplace) external onlyAdmin {
        _marketPlace = marketplace;
    }

    modifier onlyMarketplace {
        require(msg.sender == _marketPlace, "Only marketplace action");
        _;
    }

    modifier onlyAdmin {
        require(msg.sender == _admin, "Only admin action");
        _;
    }

    function mint(address to, string memory uri) external onlyMarketplace override(IERC721Mintable) {
        _tokenIdCounter.increment();
        uint256 tokenId = _tokenIdCounter.current();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }
     // The following functions are overrides required by Solidity.

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, IERC165)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
} 
