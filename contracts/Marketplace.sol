//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./NFT.sol";

contract Marketplace {
    IERC721Mintable private _nft;
    IERC20 private _token;
    address private _admin;

    event AuctionStarted(uint256 tokenId, uint256 price, address owner);
    event AuctionFinished(address winner, uint256 price);
    event NewBid(uint256 tokenId, uint256 price, address bidder);

    // tokenID => price in ERC20
    mapping(uint256 => uint256) private _listItems;
    // tokenID => lister
    mapping(uint256 => address) private _listers;

    struct Auction {
        uint256 startTime;
        uint256 currentPrice;
        uint256 totalBids;
        address owner;
        address currentBidder;
    }
    // Auction tokenId => Auction
    mapping(uint256 => Auction) public auctions;

    // Default is 259200 seconds which is 3 days
    uint256 public _minAuctionDuration = 60 * 60 * 24 * 3;
    uint256 public _minBiddersCount = 2;

    constructor(address _erc20Token, address nft) {
        _nft = IERC721Mintable(nft);
        _token = IERC20(_erc20Token);
        _admin = msg.sender;
    }

    modifier onlyAdmin {
        require(msg.sender == _admin, "Only admin action");
        _;
    }

    function updateMinAuctionDuration(uint256 _duration) external onlyAdmin {
        _minAuctionDuration = _duration;
    }

    function updateMinBiddersCount(uint256 _count) external onlyAdmin {
        _minBiddersCount = _count;
    }

    function createItem(string memory _tokenURI) external {
        _nft.mint(msg.sender, _tokenURI);
    }

    // Start Simple p2p
    function listItem(uint256 _tokenId, uint256 _price) external {
        require(_price > 0, "Zero price");
        require(_nft.ownerOf(_tokenId) == msg.sender, "Only owner of token can list");
        
        _nft.transferFrom(msg.sender, address(this), _tokenId);
        _listItems[_tokenId] = _price;
        _listers[_tokenId] = msg.sender;
        // TODO Add event
    }

    function listForToken(uint256 _tokenId) external view returns(address, uint256) {
        require(_listers[_tokenId] != address(0), "Token with given id not listed");
        return (_listers[_tokenId], _listItems[_tokenId]);
    }

    function cancel(uint256 _tokenId) external {
        require(_listers[_tokenId] == msg.sender, "Only lister can cancel");

        _nft.transferFrom(address(this), msg.sender, _tokenId);
        _clearLists(_tokenId);
    }

    function buyItem(uint256 _tokenId) external {
        require(_listers[_tokenId] != address(0), "Cannot buy not listed or already buyed item");

        _token.transferFrom(msg.sender, _listers[_tokenId], _listItems[_tokenId]);
        _nft.transferFrom(address(this), msg.sender, _tokenId);
        _clearLists(_tokenId);
        // TODO Add event
    }

    function _clearLists(uint256 _tokenId) internal {
        delete _listItems[_tokenId];
        delete _listers[_tokenId];
    }
    // End Simple p2p

    // Start auction logic
    function listItemOnAuction(uint256 _tokenId, uint256 _minPrice) external {
        require(_minPrice > 0, "Zero price");
        require(_nft.ownerOf(_tokenId) == msg.sender, "Only owner of token can start auction");

        _nft.transferFrom(msg.sender, address(this), _tokenId);
        auctions[_tokenId] = Auction({
            startTime: block.timestamp,
            currentPrice: _minPrice,
            totalBids: 0,
            owner: msg.sender,
            currentBidder: address(0)
        });
        emit AuctionStarted(_tokenId, _minPrice, msg.sender);
    }

    function makeBid(uint256 _tokenId, uint256 _price) external {
        Auction memory _auction = auctions[_tokenId];
        require(_auction.startTime != 0, "Auction for token not exist");
        require(_auction.startTime + _minAuctionDuration >= block.timestamp, "Auction is closed");
        require(_auction.currentBidder != msg.sender, "Can't bid twice in a row");
        require(_auction.currentPrice < _price, "Price must be bigger than current price");

        if (_auction.currentBidder != address(0)) {
            // Return tokens to previous bidder
            _token.transfer(_auction.currentBidder, _auction.currentPrice);
        }

        _token.transferFrom(msg.sender, address(this), _price);
        _auction.currentPrice = _price;
        _auction.currentBidder = msg.sender;
        _auction.totalBids += 1;

        auctions[_tokenId] = _auction;
        emit NewBid(_tokenId, _price, msg.sender);
    }

    function finishAuction(uint256 _tokenId) external {
        Auction memory _auction = auctions[_tokenId];

        require(_auction.startTime != 0, "Auction for token not exist");
        require(_auction.startTime + _minAuctionDuration <= block.timestamp, "Min duration for auction not pass");
        if (_auction.totalBids < _minBiddersCount) {
            if (_auction.currentBidder != address(0)) {
                // Return tokens to last bidder
                _token.transfer(_auction.currentBidder, _auction.currentPrice);
            }
            // Return NFT to owner
            _nft.transferFrom(address(this), _auction.owner, _tokenId);
            delete auctions[_tokenId];
            emit AuctionFinished(address(0), _auction.currentPrice);
            return;
        }

        _token.transfer(_auction.owner, _auction.currentPrice);
        _nft.transferFrom(address(this), _auction.currentBidder, _tokenId);

        delete auctions[_tokenId];
        emit AuctionFinished(_auction.currentBidder, _auction.currentPrice);
    }
    // End auction logic
}