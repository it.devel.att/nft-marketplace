import { ethers } from "hardhat";
import { utils, BigNumber } from "ethers";

async function main() {
  const initialTokenBalance: BigNumber = utils.parseUnits("1000000000", 18);
  const TokenA = await ethers.getContractFactory("Token");
  const erc20Token = await TokenA.deploy(
      "Token",
      "TKN",
      initialTokenBalance
    );

  const TokenB = await ethers.getContractFactory("NFT");
  const nftToken = await TokenB.deploy();

  const MarketplaceContract = await ethers.getContractFactory("Marketplace");
  const marketplace = await MarketplaceContract.deploy(erc20Token.address, nftToken.address);

  await nftToken.setMarketplace(marketplace.address);

  console.log("NFT deployed to:", nftToken.address);
  console.log("ERC20 deployed to:", erc20Token.address);
  console.log("Marketplace deployed to:", marketplace.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
