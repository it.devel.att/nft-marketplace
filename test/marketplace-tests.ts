import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract, utils, BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Token } from "../typechain";

describe("Marketplace", function () {
  const zeroAddress = '0x0000000000000000000000000000000000000000';

  let erc20Token: Contract;
  let nftToken: Contract;
  let marketplace: Contract;

  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addr4: SignerWithAddress;
  let addrs: SignerWithAddress[];
  let clean: any;

  const initialTokenBalance: BigNumber = utils.parseUnits("100000000", 18);
  const minAuctionDuration: number = 60 * 60 * 24 * 3;
  const minBiddersCount: number = 2; 
  const defaultAddressesBalance: BigNumber = utils.parseUnits("1000", 18);

  before(async () => {
    const TokenA = await ethers.getContractFactory("Token");
    erc20Token = await TokenA.deploy(
      "Token",
      "TKN",
      initialTokenBalance
    );

    const TokenB = await ethers.getContractFactory("NFT");
    nftToken = await TokenB.deploy();

    const MarketplaceContract = await ethers.getContractFactory("Marketplace");
    marketplace = await MarketplaceContract.deploy(erc20Token.address, nftToken.address);

    await nftToken.setMarketplace(marketplace.address);

    [owner, addr1, addr2, addr3, addr4, ...addrs] = await ethers.getSigners();
    await erc20Token.transfer(addr1.address, defaultAddressesBalance);
    await erc20Token.transfer(addr2.address, defaultAddressesBalance);
    await erc20Token.transfer(addr3.address, defaultAddressesBalance);
    await erc20Token.transfer(addr4.address, defaultAddressesBalance);

    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  async function increaseTime(addSeconds: number) {
    await network.provider.request({
      method: "evm_increaseTime",
      params: [addSeconds],
    });
    await network.provider.request({
      method: "evm_mine",
      params: [],
    });
  }

  async function mintNft(address: SignerWithAddress, uri: string) {
    await marketplace.connect(address).createItem(uri);
  }

  async function listItem(addr: SignerWithAddress, tokenID: number, price: number) {
    await mintNft(addr, "some-uri");
    await nftToken.connect(addr).approve(marketplace.address, tokenID);
    await marketplace.connect(addr).listItem(tokenID, price);
  }

  async function listItemOnAuction(addr: SignerWithAddress, tokenID: number, price: number) {
    await mintNft(addr, "some-uri");
    await nftToken.connect(addr).approve(marketplace.address, tokenID);
    await expect(marketplace.connect(addr).listItemOnAuction(tokenID, price))
      .to.emit(marketplace, "AuctionStarted").withArgs(
        utils.parseUnits(tokenID.toString(), 0), 
        utils.parseUnits(price.toString(), 0), 
        addr.address,
        );
    expect(await nftToken.ownerOf(tokenID)).to.eq(marketplace.address);
  }

  async function latestBlockTimestamp(): Promise<BigNumber> {
    const latestBlock = await ethers.provider.getBlock("latest");
    return utils.parseUnits(latestBlock.timestamp.toString(), 0);
  }

  async function makeBid(addr: SignerWithAddress, tokenID: number, amount: number) {
    await erc20Token.connect(addr).approve(marketplace.address, amount);
    await marketplace.connect(addr).makeBid(tokenID, amount);
  }

  async function assertAuction(
    tokenID: number,
    startTime: string,
    currentPrice: string,
    totalBids: string,
    ownerAddress: string,
    currentBidderAddress: string,
  ) {
    const auction = await marketplace.auctions(tokenID);
    expect(auction.startTime.toString()).to.eq(startTime);
    expect(auction.currentPrice.toString()).to.eq(currentPrice);
    expect(auction.totalBids.toString()).to.eq(totalBids);
    expect(auction.owner).to.eq(ownerAddress);
    expect(auction.currentBidder).to.eq(currentBidderAddress);
  }

  it("Admin can update _minAuctionDuration",async () => {
    expect((await marketplace._minAuctionDuration()).toString()).to.eq(minAuctionDuration.toString());
    await marketplace.updateMinAuctionDuration(1000);
    expect((await marketplace._minAuctionDuration()).toString()).to.eq("1000");
  });

  it("Admin can update _minBiddersCount",async () => {
    expect((await marketplace._minBiddersCount()).toString()).to.eq(minBiddersCount.toString());
    await marketplace.updateMinBiddersCount(100);
    expect((await marketplace._minBiddersCount()).toString()).to.eq("100");
  });

  it("Not admin can't update _minAuctionDuration",async () => {
    await expect(marketplace.connect(addr1).updateMinAuctionDuration(1000)).to.be.revertedWith("Only admin action");
  });

  it("Not admin can't update _minBiddersCount",async () => {
    await expect(marketplace.connect(addr1).updateMinBiddersCount(1000)).to.be.revertedWith("Only admin action");
  });

  it("Mint new nft for user",async () => {
      expect((await nftToken.balanceOf(owner.address)).toString()).to.eq("0");

      await mintNft(owner, "some-uri");

      expect((await nftToken.balanceOf(owner.address)).toString()).to.eq("1");
  });

  it("Can't list item with zero price",async () => {
    await mintNft(owner, "some-uri");

    await expect(marketplace.listItem(1, 0)).to.be.revertedWith("Zero price");
  });

  it("Can't list not owned nft",async () => {
    await mintNft(owner, "some-uri");
    await mintNft(addr1, "some-uri");

    await expect(marketplace.listItem(2, 100)).to.be.revertedWith("Only owner of token can list");
  });

  it("Can't list token if not approve it to marketplace",async () => {
    await mintNft(owner, "some-uri");

    await expect(marketplace.listItem(1, 100)).to.be.revertedWith("ERC721: transfer caller is not owner nor approved");
  });

  it("Can list token if approve nft to marketplace and owner of nft",async () => {
    await mintNft(owner, "some-uri");
    await nftToken.approve(marketplace.address, 1);

    await marketplace.listItem(1, 100);
    const [listAddress, itemPrice] = await marketplace.listForToken(1);
    expect(listAddress).to.eq(owner.address);
    expect(itemPrice.toString()).to.eq("100");
  });

  it("Only owner of list can cancel it",async () => {
    await listItem(addr1, 1, 100);

    await expect(marketplace.cancel(1)).to.be.revertedWith("Only lister can cancel");
  });

  it("While cancel marketplace return nft to lister and clear lists",async () => {
    await listItem(addr1, 1, 100);
    expect(await nftToken.ownerOf(1)).to.eq(marketplace.address);

    await marketplace.connect(addr1).cancel(1);

    expect(await nftToken.ownerOf(1)).to.eq(addr1.address);
    await expect(marketplace.listForToken(1)).to.be.revertedWith("Token with given id not listed");
  });

  it("Cannot buy not listed item",async () => {
    await expect(marketplace.buyItem(1)).to.be.revertedWith("Cannot buy not listed or already buyed item");
  });

  it("Cannot buy item if not aprroved required amount of tokens to marketplace",async () => {
    await listItem(addr1, 1, 100);

    await expect(marketplace.buyItem(1)).to.be.revertedWith("ERC20: insufficient allowance");
  });

  it("Success buy item with swap erc20 on nft and remove token from listed items",async () => {
    await listItem(addr1, 1, 100);
    expect(await nftToken.ownerOf(1)).to.eq(marketplace.address);
    expect((await erc20Token.balanceOf(addr1.address)).toString()).to.eq(defaultAddressesBalance.toString());
    const ownerERC20Balance = await erc20Token.balanceOf(owner.address);

    await erc20Token.approve(marketplace.address, 100);
    await marketplace.buyItem(1);

    const ownerERC20BalanceNew = await erc20Token.balanceOf(owner.address);

    expect(await nftToken.ownerOf(1)).to.eq(owner.address);
    expect((await erc20Token.balanceOf(addr1.address)).toString()).to.eq(defaultAddressesBalance.add(utils.parseUnits("100", 0)));
    expect(ownerERC20Balance.sub(ownerERC20BalanceNew).toString()).to.eq("100");
    await expect(marketplace.listForToken(1)).to.be.revertedWith("Token with given id not listed");
  });

  // Auction tests
  it("Can't list item on auction with zero price", async () => {
    await expect(marketplace.listItemOnAuction(1, 0)).to.be.revertedWith("Zero price");
  });

  it("Can't list item on auction with zero price", async () => {
    await mintNft(addr1, "some-uri");

    await expect(marketplace.listItemOnAuction(1, 100)).to.be.revertedWith("Only owner of token can start auction");
  });

  it("Can't list item on auction if not approve nft on marketplace", async () => {
    await mintNft(addr1, "some-uri");

    await expect(marketplace.connect(addr1).listItemOnAuction(1, 100)).to.be.revertedWith("ERC721: transfer caller is not owner nor approved");
  });

  it("Success list item on auction and freeze nft on marketplace", async () => {
    await listItemOnAuction(addr1, 1, 100);
    const latestTimestamp = await latestBlockTimestamp();

    await assertAuction(
      1,
      latestTimestamp.toString(),
      "100",
      "0",
      addr1.address,
      zeroAddress,
    );
  });

  it("Can't bid for unexist auction", async () => {
    await expect(marketplace.makeBid(99, 100)).to.be.revertedWith("Auction for token not exist");
  });

  it("Can't bid for closed auction", async () => {
    await listItemOnAuction(addr1, 1, 100);
    await increaseTime(minAuctionDuration)

    await expect(marketplace.makeBid(1, 100)).to.be.revertedWith("Auction is closed");
  });

  it("Can't bid twice in a row", async () => {
    await listItemOnAuction(addr1, 1, 100);

    await erc20Token.approve(marketplace.address, 101);
    await marketplace.makeBid(1, 101);

    await expect(marketplace.makeBid(1, 100)).to.be.revertedWith("Can't bid twice in a row");
  });

  it("Can't bid with price less then or equal current price", async () => {
    await listItemOnAuction(addr1, 1, 100);

    await expect(marketplace.makeBid(1, 99)).to.be.revertedWith("Price must be bigger than current price");
    await expect(marketplace.makeBid(1, 100)).to.be.revertedWith("Price must be bigger than current price");
  });

  it("Success bid", async () => {
    await listItemOnAuction(addr1, 1, 100);
    const latestTimestamp = await latestBlockTimestamp();

    const erc20BalanceBefore = await erc20Token.balanceOf(owner.address);

    await erc20Token.approve(marketplace.address, 101);
    await marketplace.makeBid(1, 101);

    const erc20BalanceAfter = await erc20Token.balanceOf(owner.address);

    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("101");
    expect(erc20BalanceBefore.sub(erc20BalanceAfter).toString()).to.eq("101");

    await assertAuction(
      1,
      latestTimestamp.toString(),
      "101",
      "1",
      addr1.address,
      owner.address,
    );
  });

  it("Success bid with return coins to previous bidder", async () => {
    await listItemOnAuction(addr1, 1, 100);
    const latestTimestamp = await latestBlockTimestamp();

    const ownerERC20BalanceBefore = await erc20Token.balanceOf(owner.address);

    await erc20Token.approve(marketplace.address, 101);
    await marketplace.makeBid(1, 101);

    const ownerERC20BalanceAfter = await erc20Token.balanceOf(owner.address);

    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("101");
    expect(ownerERC20BalanceBefore.sub(ownerERC20BalanceAfter).toString()).to.eq("101");
    await assertAuction(
      1,
      latestTimestamp.toString(),
      "101",
      "1",
      addr1.address,
      owner.address,
    );

    await erc20Token.connect(addr2).approve(marketplace.address, 102);
    await expect(marketplace.connect(addr2).makeBid(1, 102))
      .to.emit(marketplace, "NewBid").withArgs(utils.parseUnits("1", 0), utils.parseUnits("102", 0), addr2.address);

    await assertAuction(
      1,
      latestTimestamp.toString(),
      "102",
      "2",
      addr1.address,
      addr2.address,
    );
    expect((await erc20Token.balanceOf(owner.address)).toString()).to.eq(ownerERC20BalanceBefore.toString());
    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("102");
  });

  it("Can't finish unexist auction", async () => {
    await expect(marketplace.finishAuction(1)).to.be.revertedWith("Auction for token not exist");
  });

  it("Can't finish auction if required time not pass", async () => {
    await listItemOnAuction(addr1, 1, 100);

    await expect(marketplace.finishAuction(1)).to.be.revertedWith("Min duration for auction not pass");
  });

  it("Finish auction without bidders", async () => {
    await listItemOnAuction(addr1, 1, 100);
    await increaseTime(minAuctionDuration);

    expect(await nftToken.ownerOf(1)).to.eq(marketplace.address);
    await marketplace.finishAuction(1);
    expect(await nftToken.ownerOf(1)).to.eq(addr1.address);
  });

  it("Finish auction with less bidders than required return tokens to last bidder", async () => {
    await listItemOnAuction(addr1, 1, 100);
    const balanceBeforeBid = await erc20Token.balanceOf(addr2.address);
    await makeBid(addr2, 1, 101);
    const balanceAfterBid = await erc20Token.balanceOf(addr2.address);
    expect(balanceBeforeBid.sub(balanceAfterBid)).to.eq("101");

    await increaseTime(minAuctionDuration);

    expect(await nftToken.ownerOf(1)).to.eq(marketplace.address);
    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("101");

    await expect(marketplace.finishAuction(1))
      .to.emit(marketplace, "AuctionFinished").withArgs(zeroAddress, utils.parseUnits("101", 0));

    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("0");
    expect((await erc20Token.balanceOf(addr2.address)).toString()).to.eq(balanceBeforeBid.toString());
    expect(await nftToken.ownerOf(1)).to.eq(addr1.address);
    await assertAuction(
      1,
      "0",
      "0",
      "0",
      zeroAddress,
      zeroAddress,
    );
  });

  it("Finish auction with pass required bidders value", async () => {
    const auctionOwnerBalanceBefore = await erc20Token.balanceOf(addr1.address);

    await listItemOnAuction(addr1, 1, 100);
    await makeBid(addr2, 1, 101);
    await makeBid(addr3, 1, 200);
    await increaseTime(minAuctionDuration);

    await expect(marketplace.finishAuction(1))
      .to.emit(marketplace, "AuctionFinished").withArgs(addr3.address, utils.parseUnits("200", 0));

    expect((await erc20Token.balanceOf(marketplace.address)).toString()).to.eq("0");
    expect((await erc20Token.balanceOf(addr1.address)).toString()).to.eq(auctionOwnerBalanceBefore.add(utils.parseUnits("200", 0)));
    expect(await nftToken.ownerOf(1)).to.eq(addr3.address);
    await assertAuction(
      1,
      "0",
      "0",
      "0",
      zeroAddress,
      zeroAddress,
    );
  });
});
